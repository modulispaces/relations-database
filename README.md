# Faber-Zagier relations database for admcycles

This is a database with pre-computed Faber-Zagier relations to be used with the [admcycles](https://gitlab.com/modulispaces/admcycles/) SageMath package.

[Here](https://modulispaces.gitlab.io/relations-database/index.html) you can find an overview of the data currently available in this repository. Contributions are highly welcome.

New files should be added using the git LFS extension. After installing git LFS on your system, run `git lfs install` inside this repository once. Afterwards you can commit your files as usual.
