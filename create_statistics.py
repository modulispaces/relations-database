#!/usr/bin/python

from collections import defaultdict
import pickle
import bz2
import os

header = r"""
<html>
<head>
    <title>FZ relations database for admcycles</title>
    <style>
        body {text-align: center;}
        table {text-align: center; margin-left: auto; margin-right: auto}
        table, td, th {border: 1px solid black; border-collapse: collapse;}
        th, td {padding: 5pt;}
        .good {color: green;}
        .bad {color: red;}
        .unknown {color: orange;}
    </style>
</head>
<body>
<h1>Faber-Zagier relations database for admcycles</h1>
<p>This is a list of all Faber-Zagier relations that are currently available in the admcycles online database. The table below also indicates whether the Faber-Zagier conjecture (i.e. the completeness of the relations) has been confirmed for the moduli space of stable curves.
The numbers in parentheses are the Betti numbers.</p>
"""

footer = r"""
</body>
</html>
"""

class GeneratingIndices:
    def __init__(self, directory, filename):
        _, _, g, n, r, moduli_ext = filename.split("_")
        self.moduli, ext1, ext2 = moduli_ext.split(".")
        assert ext1 == "pkl" and ext2 == "bz2"
        self.g = int(g)
        self.n = int(n)
        self.r = int(r)
        f = bz2.open(os.path.join(directory, filename), "rb")
        self.betti = len(pickle.load(f))

class Genstobasis:
    def __init__(self, filename):
        _, g, n, r, moduli_ext = filename.split("_")
        self.moduli, ext1, ext2 = moduli_ext.split(".")
        assert ext1 == "pkl" and ext2 == "bz2"
        self.g = int(g)
        self.n = int(n)
        self.r = int(r)

class FZHolds:
    def __init__(self, directory, filename):
        _, _, _, g, n, r, method_ext = filename.split("_")
        _, ext1, ext2 = method_ext.split(".")
        assert ext1 == "pkl" and ext2 == "bz2"
        self.g = int(g)
        self.n = int(n)
        self.r = int(r)
        f = bz2.open(os.path.join(directory, filename), "rb")
        self.holds = pickle.load(f)

def good():
    return "<span class=\"good\">&#10004;</span>\n"

def bad():
    return "<span class=\"bad\">&#10060;</span>\n"

def unknown():
    return "<span class=\"unknown\">?</span>\n"

def get_data(directory):
    gi = defaultdict(list)
    gtb = defaultdict(list)
    fz = defaultdict(list)
    for f in os.listdir(directory):
        if f.startswith("generating_indices"):
            tmp = GeneratingIndices(directory, f)
            gi[tmp.g].append(tmp)
        if f.startswith("genstobasis"):
            tmp = Genstobasis(f)
            gtb[tmp.g].append(tmp)
        if f.startswith("FZ_conjecture_holds"):
            tmp = FZHolds(directory, f)
            fz[tmp.g].append(tmp)
    return (gi, gtb, fz)

def print_page(gi, gtb, fz):
    f = open("index.html", "w")
    f.write(header)
    f.write("<table>")
    f.write("<tr>")
    f.write("<th>g</th>");
    f.write("<th>n</th>");
    f.write("<th>r</th>");
    f.write("<th>st</th>");
    f.write("<th>ct</th>");
    f.write("<th>rt</th>");
    f.write("<th>sm</th>");
    f.write("<th>FZ conjecture holds</th>");
    f.write("</tr>")

    max_g = max(list(gi.keys()) + list(gtb.keys()) + list(fz.keys()))
    for g in range(max_g + 1):
        f.write(get_g_row(g, gi[g], gtb[g], fz[g]))
    f.write("</table>")
    f.write(footer)

def get_g_row(g, gi_g, gtb_g, fz_g):
    # First order the things into dictionaries indexed by n
    def group_by_n(l):
        res = defaultdict(list)
        for d in l:
            res[d.n].append(d)
        return res
    gi_g = group_by_n(gi_g)
    gtb_g = group_by_n(gtb_g)
    fz_g = group_by_n(fz_g)
    n_list = list(gi_g.keys()) + list(gtb_g.keys()) + list(fz_g.keys())
    if not n_list:
        return ""
    max_n = max(n_list)
    g_row = ""
    total_rows = 0
    first_row = True
    if g == 0:
        min_n = 3
    elif g == 1:
        min_n = 1
    else:
        min_n = 0
    for n in range(min_n, max_n + 1):
        rows, n_row = get_n_row(g, n, gi_g[n], gtb_g[n], fz_g[n])
        total_rows += rows
        if first_row:
            first_row = False
            g_row += "\n<!--N ROW-->\n" + n_row
        else:
            g_row += "\n<!--N ROW-->\n<tr>" + n_row
    g_row = "\n<!--G ROW-->\n<tr><td rowspan=\"" + str(total_rows) + "\">" + str(g) + "</td>\n" + g_row + "\n"
    return g_row
    
def get_n_row(g, n, gi_gn, gtb_gn, fz_gn):
    # First order the things into dictionaries indexed by r
    def group_by_r(l):
        res = defaultdict(list)
        for d in l:
            res[d.r].append(d)
        return res
    gi_gn = group_by_r(gi_gn)
    gtb_gn = group_by_r(gtb_gn)
    fz_gn = group_by_r(fz_gn)
    max_r = 3*g-3+n
    n_row = ""
    total_rows = 0
    first_row = True
    all_moduli = ["st", "ct", "rt", "sm"]
    def check_moduli(l):
        moduli = [d.moduli for d in l]
        return {m: m in moduli for m in all_moduli}
    for r in range(max_r + 1):
        if first_row:
            first_row = False
            n_row += "\n<!--R ROW-->\n"
        else:
            n_row += "\n<!--R ROW-->\n<tr>\n"
        n_row += "<td>" + str(r) + "</td>\n"
        moduli_gi = check_moduli(gi_gn[r])
        moduli_gtb = check_moduli(gtb_gn[r])
        for m in all_moduli:
            if moduli_gi[m] and moduli_gtb[m]:
                betti = None
                for d in gi_gn[r]:
                    if d.moduli == m:
                        betti = d.betti
                        break
                assert betti is not None
                n_row += "<td>" + good() + "(" + str(betti)  + ")</td>\n"
            else:
                n_row += "<td>" + bad() + "</td>\n"
        if any(f.holds for f in fz_gn[r]):
            n_row += "<td>" + good() + "</td>\n"
        else:
            n_row += "<td>" + unknown() + "</td>\n"
        n_row += "</tr>\n"
        total_rows += 1
    if total_rows != 0:
        n_row = "<td rowspan=\"" + str(total_rows) + "\">" + str(n) + "</td>\n" + n_row
    else:
        n_row = ""
    return total_rows, n_row

if __name__ == "__main__":
    directory = os.path.join(os.environ["CI_PROJECT_DIR"], "data")
    data = get_data(directory)
    print_page(*data)
